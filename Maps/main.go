package main

import "fmt"

func main() {
	//To create an empty map, use the builtin make: make(map[key-type]val-type).
	m := make(map[string]int)
	fmt.Println("EmptyMap: ", m)

	//Set key/value pairs using typical name[key] = val syntax.
	m["k1"] = 1
	m["k2"] = 2
	m["k3"] = 3

	fmt.Println("Set: ", m)

	//Get a value for a key with name[key].
	v1 := m["k1"]
	fmt.Println("Value1: ", v1)

	//The builtin len returns the number of key/value pairs when called on a map.
	fmt.Println("len: ", len(m))

	//The builtin delete removes key/value pairs from a map.
	delete(m, "k2")
	fmt.Println("After Delete: ", m)

	//To check whether a key is present or not
	_, prs := m["k3"]
	fmt.Println("Miss: ", prs)

	//You can also declare and initialize a new map in the same line with this syntax.
	n := map[string]int{"foo": 1, "bar":2}
	fmt.Println("dcl: ", n)
}