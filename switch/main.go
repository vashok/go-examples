package main

import (
	"fmt"
	"time"
)

func main() {
	i := 2
	fmt.Print("Write ", i, " as ")
	
	switch i {
	case 1 :
		fmt.Print("one")
	case 2:
		fmt.Print("two")
	case 3:
		fmt.Print("three")
	}

	//You can use commas to separate multiple expressions in the same case statement.
	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("It is the weekend")
	default:
		fmt.Println("It is a weekday")
	}

	//switch without an expression is an alternate way to express if/else logic
	t := time.Now()
	switch {
		case t.Hour() < 12:
			fmt.Println("It is before noon")
		default:
			fmt.Println("It is after noon")
	}

	//A type switch compares types instead of values. You can use this to discover the type of an interface value
	whatIam := func(i interface{}) {
		switch t := i.(type) {
		case bool :
			fmt.Println("I am a boolean")
		case int:
			fmt.Println("I am an Integer")
		default:
			fmt.Printf("Don't know type %T\n", t)
		}
	}

	whatIam(true)
	whatIam(1)
	whatIam("hey")
}