package main

import "fmt"

//Here’s a function that takes two ints and returns their sum as an int.
func plus(a,b int) int {
	return a + b
}

func main() {
	res := plus(1,2)
	fmt.Println("Result: ", res)
}