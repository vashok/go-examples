package main

import "fmt"

func main() {

	if 7 % 2 == 0 {
		fmt.Println("7 is Even")
	} else {
		fmt.Println("7 is Odd")
	}

	if  9 % 3 == 0 {
		fmt.Println("9 is divisible by 3")
	}

	if num := 9; num < 0 {
		fmt.Println("Number is negative")
	} else if num < 10 {
		fmt.Println("Number has only one digit")
	} else {
		fmt.Println("Number has multiple digits")
	}
}