package main

import "fmt"

func main() {
	nums := []int{1,2,3,4,5}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println("sum: ", sum) 

	//usage of index
	for index, num := range nums {
		if num == 3 {
			fmt.Println("index: ", index)
		}
	}

	m := map[string]int{"k1":1, "k2":2, "k3":3}
	for k, v := range m {
		fmt.Printf("%s -> %d\n", k,v)
	}

	for k := range m {
		fmt.Println("key: ",k)
	}

	//range on strings iterates over Unicode code points. 
	//The first value is the starting byte index of the rune and the second the rune itself.
	for i, c := range "abcdABCD" {
		fmt.Println(i,c)
	}
}